import logging
import azure.functions as func
import math

def abs_sin(x : float) -> float:
    
    return abs(math.sin(x))

def compute_integral(N : int, minInterval : float,
                     maxInterval : float) -> float:
    
    result = 0.0;
    dx = (maxInterval - minInterval) / N

    for i in range(N):
        xip12 = dx * (i + 0.5)
        dI = abs_sin(xip12) * dx
        result += dI
    
    return result

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    minInterval = req.route_params.get('minInterval')
    maxInterval = req.route_params.get('maxInterval')

    if minInterval != None and maxInterval != None:
        result = compute_integral(1000000, float(minInterval), float(maxInterval))
        return func.HttpResponse(f"The obtained result is {str(result)}.")
    
    else:
        return func.HttpResponse(
             "Welcome to this numerical integration function service realised by Brahim Bouallagui. You can use the route /api/minInterval/maxInterval to do your computation. Example: /api/0/3.14159",
             status_code=200)
