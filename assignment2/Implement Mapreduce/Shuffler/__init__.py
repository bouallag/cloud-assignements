# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging

def main(keyValueList: list) -> tuple:
    words = []
    counts = []

    for line in keyValueList:
        for word in line:
            if word[0] not in words:
                words.append(word[0])
                counts.append([1])
            else:
                counts[words.index(word[0])].append(1)
            
    return list(zip(words, counts))
